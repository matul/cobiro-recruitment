<?php

namespace App\Model\Service;

use App\Model\Entities\Product;
use App\Model\Repository\ProductRepositoryInterface;

class ProductCreationService
{
    private ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function saveProduct(Product $product)
    {
        $this->productRepository->saveProduct($product);
    }
}