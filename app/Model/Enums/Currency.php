<?php


namespace App\Model\Enums;

use MyCLabs\Enum\Enum;

class Currency extends Enum
{
    const USD = 'usd';
}