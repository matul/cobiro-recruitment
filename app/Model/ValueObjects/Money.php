<?php


namespace App\Model\ValueObjects;

use App\Model\Enums\Currency;
use App\Model\Exceptions\InvalidAmountException;

class Money
{
    private int $amount;
    private Currency $currency;

    /**
     * @param int $amount The amount in decimal units (f. ex. cents for dollars)
     * @param Currency $currency
     */
    public function __construct(int $amount, Currency $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return int The amount in decimal units (f. ex. cents for dollars)
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return float
     */
    public function asFloat(): float
    {
        return $this->amount / 100;
    }

    /**
     * We make the assumption that every currency is divided by 100
     * @return int The amount in whole units (f. ex. 3 for 3 dollars and 50 cents)
     */
    public function getWholeUnits(): int
    {
        return floor($this->asFloat());
    }

    /**
     * We make the assumption that every currency is divided by 100
     * @return int The amount in decimal units (f. ex. 50 for 3 dollars and 50 dents)
     */
    public function getDecimalUnits(): int
    {
        return $this->amount % 100;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public static function fromFloat(float $amount, Currency $currency)
    {
        $decimalAmount = $amount * 100;
        $roundedAmount = floor($decimalAmount);

        if ($roundedAmount !== $decimalAmount) {
            throw new InvalidAmountException($amount);
        }

        return new self($roundedAmount, $currency);
    }
}
