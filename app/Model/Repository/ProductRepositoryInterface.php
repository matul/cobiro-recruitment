<?php

namespace App\Model\Repository;

use App\Model\Entities\Product;

interface ProductRepositoryInterface
{
    public function saveProduct(Product $product);
}