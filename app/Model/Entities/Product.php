<?php

namespace App\Model\Entities;

use App\Model\ValueObjects\Money;
use Ramsey\Uuid\UuidInterface;

class Product
{
    private UuidInterface $id;
    private Money $price;
    private string $name;

    public function __construct(UuidInterface $id, Money $price, string $name)
    {
        $this->id = $id;
        $this->price = $price;
        $this->name = $name;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getPriceAsFloat(): float
    {
        return $this->price->asFloat();
    }

    /**
     * @return int The amount in decimal units (f. ex. cents for dollars)
     */
    public function getPriceDecimal(): int
    {
        return $this->price->getAmount();
    }

    public function getCurrencyString(): string
    {
        return $this->price->getCurrency()->getKey();
    }

    public function getName(): string
    {
        return $this->name;
    }
}
