<?php


namespace App\Model\Exceptions;

class InvalidAmountException extends ModelException
{
    public function __construct(float $amount)
    {
        parent::__construct("Provided money amount is invalid: $amount");
    }
}