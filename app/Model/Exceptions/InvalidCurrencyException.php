<?php


namespace App\Model\Exceptions;


class InvalidCurrencyException extends ModelException
{
    public function __construct(string $providedCurrency)
    {
        parent::__construct("Invalid or unknown currency: \"$providedCurrency\"");
    }
}
