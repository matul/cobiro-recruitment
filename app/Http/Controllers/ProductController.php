<?php

namespace App\Http\Controllers;

use App\Http\Services\RequestFactories\RequestProductFactory;
use App\Http\Services\ResponseFactories\ProductResponseFactory;
use App\Model\Service\ProductCreationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductController extends \Illuminate\Routing\Controller
{
    private RequestProductFactory $requestProductFactory;
    private ProductResponseFactory $productResponseFactory;
    private ProductCreationService $productCreationService;

    public function __construct(
        RequestProductFactory $requestProductFactory,
        ProductResponseFactory $productResponseFactory,
        ProductCreationService $productCreationService
    )
    {
        $this->requestProductFactory = $requestProductFactory;
        $this->productResponseFactory = $productResponseFactory;
        $this->productCreationService = $productCreationService;
    }

    public function create(Request $request): JsonResponse
    {
        $product = $this->requestProductFactory->createProduct($request);
        $this->productCreationService->saveProduct($product);

        return $this->productResponseFactory->createResponse(
            $product,
            201
        );
    }
}
