<?php


namespace App\Http\Services\RequestFactories;


use App\Model\Entities\Product;
use App\Model\Enums\Currency;
use App\Model\ValueObjects\Money;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RequestProductFactory
{
    public function createProduct(Request $request)
    {
        $priceAmount = $request->json('price.amount');
        $priceCurrency = $request->json('price.currency');
        $name = $request->json('name');

        if (!$priceAmount || !$priceCurrency || !$name) {
            throw new BadRequestHttpException('This endpoint requires \'name\' and \'price\' object containing amount and currency.');
        }

        return new Product(
            Uuid::uuid4(),
            $this->createPrice($request),
            $request->json('name')
        );
    }

    /**
     * @param Request $request
     */
    private function createPrice(Request $request): Money
    {
        $currencyString = $request->json('price.currency');
        try {
            return Money::fromFloat($request->json('price.amount'), new Currency(strtolower($currencyString)));
        } catch (\UnexpectedValueException $e) {
            throw new BadRequestHttpException("Currency $currencyString is invalid");
        }
    }
}