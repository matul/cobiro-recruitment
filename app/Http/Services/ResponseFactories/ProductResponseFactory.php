<?php


namespace App\Http\Services\ResponseFactories;


use App\Model\Entities\Product;
use Illuminate\Http\JsonResponse;

class ProductResponseFactory
{
    public function createResponse(Product $product, int $code)
    {
        return new JsonResponse([
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => [
                'amount' => $product->getPriceAsFloat(),
                'currency' => $product->getCurrencyString()
            ]
        ], $code);
    }
}