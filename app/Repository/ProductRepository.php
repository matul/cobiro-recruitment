<?php


namespace App\Repository;

use App\Model\Entities\Product;
use App\Product as ProductRecord;
use App\Model\Repository\ProductRepositoryInterface;

class ProductRepository implements ProductRepositoryInterface
{
    public function saveProduct(Product $product)
    {

        $productRecord = new ProductRecord();
        $productRecord->id = $product->getId()->toString();
        $productRecord->name = $product->getName();
        $productRecord->price = $product->getPriceDecimal();
        $productRecord->currency = $product->getCurrencyString();
        $productRecord->save();
    }
}
